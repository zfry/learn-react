from django.db import models

# Create your models here.
class Technician(models.Model):
    name = models.CharField(max_length=200)
    employee_number = models.PositiveSmallIntegerField()


class serviceAppointment(models.Model):
    vin = models.CharField(max_length=17, unique=True)
    name = models.CharField(max_length=200)
    date_time = models.DateTimeField(auto_now=True)
    technician = models.ForeignKey(
        Technician,
        related_name="+",
        on_delete=models.PROTECT
    )
    reason = models.TextField()


class AutomobileVO(models.Model):
    import_href = models.CharField(max_length=200)
    color = models.CharField(max_length=50)
    year = models.PositiveSmallIntegerField()
    vin = models.CharField(max_length=17, unique=True)