import React, {useEffect, useState} from 'react';

function Company(props) {
  //List of companies
  //Each company will have employees, profit, budget, name, inventory

  // props = {
  //   companyInfo:  {
  //     "employees": '100',
  //     "profit": '17,000',
  //     "budget": '100,000',
  //     "name": "Jackson Enterprises Inc.",
  //     "Inventory": ["Cars", "Bikes", "Boats"]
  //   }
  // }

  return (
    <tr>
      <td>{props.companyInfo.employees}</td>
      <td>{}</td>
      <td>{}</td>
      <td>{}</td>
      <td>{props.companyInfo.Inventory.join(', ')}</td>
    </tr>
  );
}

export default Company;