import React from 'react';
import ReactDOM from 'react-dom/client';
import CompanyList from './CompanyList';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <CompanyList />
  </React.StrictMode>
);
