const data = [
  {
    "employees": '100',
    "profit": '17,000',
    "budget": '100,000',
    "name": "Jackson Enterprises Inc.",
    "Inventory": ["Cars", "Bikes", "Boats"]
  },
  {
    "employees": '76',
    "profit": '12,000',
    "budget": '85,000',
    "name": "Zach Enterprises Inc.",
    "Inventory": ["German Books", "Chicken"]
  },
  {
    "employees": '253',
    "profit": '50,000',
    "budget": '600,000',
    "name": "Carlos Enterprises Inc.",
    "Inventory": ["Tacos", "Soccer Ball"]
  },
]

export default data;