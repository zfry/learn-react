import React, {useEffect, useState} from 'react';
import data from './companydata';
import Company from './Company';

function CompanyList() {
  //List of companies
  //Each company will have employees, profit, budget, name, inventory

  return (
    <div className="container">
      <table>
        <tr>
          <th>Employees</th>
          <th>Profit</th>
          <th>Budget</th>
          <th>Name</th>
          <th>Inventory</th>
        </tr>
        {data.map((co) => {
          console.log(co)
          return <Company companyInfo={co}/>
        })}
      </table>
    </div>
  );
}

export default CompanyList;
